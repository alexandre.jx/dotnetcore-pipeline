
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using Weather.Api.Controllers;
using Moq;
using System.Linq;

namespace Weather.Api.Tests
{
    public class Tests
    {

        private Mock<ILogger<WeatherForecastController>> logger;

        private WeatherForecastController sut;


        [SetUp]
        public void Setup()
        {
            logger = new Mock<ILogger<WeatherForecastController>>();
            sut = new WeatherForecastController(logger.Object);
        }

        [Test]
        public void ShouldRetureWeather()
        {
            
            var result = sut.Get();

            Assert.True(result.Count() == 5);
        }
    }
}